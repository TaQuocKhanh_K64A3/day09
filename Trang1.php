<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Chọn câu trả lời đúng nhất</h2>
    <?php
    $A_Q = array(
        '1' => array(
            'Question' => "Ngày 2-9-1945, ở Việt Nam đã diễn ra sự kiện lịch sử trọng đại gì?",
            'name' => 'cauhoi1',
            'Answer' => array(
                'A' => "Cách mạng tháng Tám thành công trên cả nước",
                'B' => "Hồ Chí Minh đọc bản Tuyên ngôn độc lập, tuyên bố sự ra đời của nước Việt Nam Dân chủ cộng hòa",
                'C' => "Vua Bảo Đại tuyên bố thoái vị",
                'D' => "Thực dân Pháp chính thức nổ súng quay trở lại xâm lược Việt Nam"
            ),
            'Ans' => "B"
        ),
        '2' => array(
            'Question' => "Nước Việt Nam có bao nhiêu tỉnh thành",
            'name' => 'cauhoi2',
            'Answer' => array(
                'A' => "60",
                'B' => "61",
                'C' => "62",
                'D' => "63"
            ),
            'Ans' => "D"
        ),
        '3' => array(
            'Question' => "Bác Hồ sinh ngày tháng năm nào?",
            'name' => 'cauhoi3',
            'Answer' => array(
                'A' => "19/05/1890",
                'B' => "15/09/1890",
                'C' => "19/05/1980",
                'D' => "15/09/1980"
            ),
            'Ans' => "A"
        ),
        '4' => array(
            'Question' => "Hang động nào lớn nhất thế giới?",
            'name' => 'cauhoi4',
            'Answer' => array(
                'A' => "Mammoth (Mỹ)",
                'B' => "Sistema Sac Actun (Mexico)",
                'C' => "Hang Sơn Đoòng(Việt Nam)",
                'D' => "Hang động Vatnajokull Glacier(Iceland)"
            ),
            'Ans' => "C"
        ),
        '5' => array(
            'Question' => "Nước nào xuất khẩu cafe lớn nhất thế giới",
            'name' => 'cauhoi5',
            'Answer' => array(
                'A' => "Brazil",
                'B' => "Việt Nam",
                'C' => "Colombia",
                'D' => "Indonesia"
            ),
            'Ans' => "A"
        ),
    );
    ?>
    <?php
    $data = array();
    $error = false;
    $cookie_name = 'trang1';
    $cookie_value = 0;
    if (!empty($_POST['submit'])) {
        $data['cauhoi1'] = isset($_POST['cauhoi1']) ? $_POST['cauhoi1'] : '';
        $data['cauhoi2'] = isset($_POST['cauhoi2']) ? $_POST['cauhoi2'] : '';
        $data['cauhoi3'] = isset($_POST['cauhoi3']) ? $_POST['cauhoi3'] : '';
        $data['cauhoi4'] = isset($_POST['cauhoi4']) ? $_POST['cauhoi4'] : '';
        $data['cauhoi5'] = isset($_POST['cauhoi5']) ? $_POST['cauhoi5'] : '';

        foreach ($A_Q as $key => $value) {
            $name = 'cauhoi' . $key;
            if ($value['Ans'] == $data[$name]) {
                $cookie_value = $cookie_value + 1;
            }
        }
        setcookie($cookie_name, $cookie_value);
        header("Location: ./Trang2.php");
    }
    ?>
    <form method="POST" action="Trang1.php">
        <?php
        foreach ($A_Q as $key => $value) {
            echo 'Question ' . $key . ': ' . $value['Question'];
            foreach ($value['Answer'] as $keyAnswer => $valueAnswer) {
                echo '<div>
                <input type="radio" id="' . $key . '' . $keyAnswer . '" name="' . $value['name'] . '" value="' . $keyAnswer . '">
                <label for="' . $keyAnswer . '">' . $valueAnswer . '</label>
                </div>';
            }
        }
        ?>
        <input type="submit" name="submit" value="Next">
    </form>
</body>

</html>