<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Chọn câu trả lời đúng nhất</h2>
    <?php
    $A_Q = array(
        '6' => array(
            'Question' => "Nước nào sạch nhất trên thế giới?",
            'name' => 'cauhoi6',
            'Answer' => array(
                'A' => "Singapore",
                'B' => "Thụy Sỹ",
                'C' => "Thụy Điển",
                'D' => "Mỹ"
            ),
            'Ans' => "C"
        ),
        '7' => array(
            'Question' => "Nước nào có quy mô nền kinh tế lớn nhất thế giới?",
            'name' => 'cauhoi7',
            'Answer' => array(
                'A' => "Đức",
                'B' => "Trung Quốc",
                'C' => "Pháp",
                'D' => "Mỹ"
            ),

            'Ans' => "D"
        ),
        '8' => array(
            'Question' => "1+1=?",
            'name' => 'cauhoi8',
            'Answer' => array(
                'A' => "1",
                'B' => "2",
                'C' => "3",
                'D' => "4"
            ),
            'Ans' => "B"
        ),
        '9' => array(
            'Question' => "1*1=?",
            'name' => 'cauhoi9',
            'Answer' => array(
                'A' => "1",
                'B' => "2",
                'C' => "3",
                'D' => "4"
            ),
            'Ans' => "A"
        ),
        '10' => array(
            'Question' => "Sự kiện báo hiệu sự toàn thắng của Chiến dịch Hồ Chí Minh lịch sử năm 1975 là",
            'name' => 'cauhoi10',
            'Answer' => array(
                'A' => "Lá cờ cách mạng tung bay trên nóc Dinh Độc lập",
                'B' => "Xe tăng bộ đội húc đổ cổng Dinh Độc Lập",
                'C' => "5 cánh quân của ta vượt qua tuyến phòng thủ vòng ngoài của địch tiến vào trung tâm Sài Gòn",
                'D' => "Tổng thống Dương Văn Minh kívăn bản đầu hàng không điều kiện"
            ),
            'Ans' => "D"
        ),
    );
    ?>
    <?php
    $data = array();
    $error = false;
    $cookie_name = 'trang2';
    $cookie_value = 0;
    if (!empty($_POST['submit'])) {
        $data['cauhoi6'] = isset($_POST['cauhoi6']) ? $_POST['cauhoi6'] : '';
        $data['cauhoi7'] = isset($_POST['cauhoi7']) ? $_POST['cauhoi7'] : '';
        $data['cauhoi8'] = isset($_POST['cauhoi8']) ? $_POST['cauhoi8'] : '';
        $data['cauhoi9'] = isset($_POST['cauhoi9']) ? $_POST['cauhoi9'] : '';
        $data['cauhoi10'] = isset($_POST['cauhoi10']) ? $_POST['cauhoi10'] : '';


        foreach ($A_Q as $key => $value) {
            $name = 'cauhoi' . $key;
            if ($value['Ans'] == $data[$name]) {
                $cookie_value = $cookie_value + 1;
            }
        }
        setcookie($cookie_name, $cookie_value);
        header("Location: ./KetQua.php");
    }
    ?>
    <form method="POST" action="Trang2.php">
        <?php
        foreach ($A_Q as $key => $value) {
            echo 'Question ' . $key . ': ' . $value['Question'];
            foreach ($value['Answer'] as $keyAnswer => $valueAnswer) {
                echo '<div>
                <input type="radio" id="' . $key . '' . $keyAnswer . '" name="' . $value['name'] . '" value="' . $keyAnswer . '">
                <label for="' . $keyAnswer . '">' . $valueAnswer . '</label>
                </div>';
            }
        }
        ?>
        <input type="submit" name="submit" value="Submit">
    </form>
</body>

</html>